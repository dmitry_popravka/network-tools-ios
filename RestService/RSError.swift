//
//  RSError.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import UIKit

public protocol RSErrorProtocol: Codable {
    func show()
}

enum RSErrorType: Error {
    
    case convertJSON(String)
    
    var message: String {
        switch self {
            case .convertJSON(let value): return "No convert json to item: \nURL ->>> \(value)"
        }
    }
}

public struct RSError {

    func parsing<E: RSErrorProtocol> (error errorModel: E) {
        errorModel.show()
    }
    
    // MARK: -
    // MARK: Failure
    
    func parsing(failure error: Error) {
    
        if let block = RSSession.shared.failureBlock {
            block(error)
        } else {
            if let callError = error as? RSErrorType {
                show(message: callError.message)
            } else {
                show(message: error.localizedDescription)
            }
        }
    }
    
    private func show(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.topViewController()?.present(alert, animated: true, completion: nil)
        }
    }
}

extension RSError {
    
    private func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        
        if base == nil {
            return UIApplication.shared.delegate?.window??.rootViewController
        }
        
        return base
    }
}
