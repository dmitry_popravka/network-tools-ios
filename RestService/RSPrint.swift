//
//  RSPrint.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import Foundation

struct RSPrint {
    
    func print(request: URLRequest,
               params: [String: Any]?,
               handler: (data: Data?, response: URLResponse?, error: Error?)) {
        
        debugPrint("==================== REQUEST ============================")
        
        debugPrint("url:", request.url?.absoluteString ?? "<url is nil>")
        debugPrint("httpMethod:", request.httpMethod ?? "<httpMethod is nil>")
        
        if let headerFields = request.allHTTPHeaderFields, !headerFields.isEmpty  {
            debugPrint("headers:", headerFields)
        } else {
            debugPrint("headers:", "<headers is empty>")
        }
        
        if let params = params, !params.isEmpty  {
            debugPrint("body:", params as AnyObject)
        } else {
            debugPrint("body:", "<body is empty>")
        }
        
        if let data = handler.data,
            let json = try? JSONSerialization.jsonObject(with: data, options : .allowFragments) {
            debugPrint("data:", json )
        } else {
            debugPrint("data:", "<data is empty>" )
        }
        
        if let error = handler.error {
            if let callError = error as? RSErrorType {
                debugPrint("error:", callError.message)
            } else {
                debugPrint("error:", error.localizedDescription)
            }
        }
        
        debugPrint("=========================================================")
        
    }
    
    func printCancelCall(whereName name: String, sessionTask: URLSessionTask) {
        debugPrint("================= CANCEL REQUEST ========================")
        debugPrint("name:", name)
        debugPrint("url:", sessionTask.response?.url?.absoluteString ?? "<url is nil>")
        debugPrint("=========================================================")
    }
    
}

