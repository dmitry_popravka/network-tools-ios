//
//  RSLoaderViewProtocol.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import UIKit

public protocol RSLoaderViewProtocol where Self: UIView {
    func configure()
    func showView()
    func hideView()
}

extension RSLoaderViewProtocol {
    
    public static func instantiateFromXib() -> Self {
        
        let metatype = Self.self
        let bundle = Bundle(for: metatype)
        let nib = UINib(nibName: "\(metatype)", bundle: bundle)
        
        guard let view = nib.instantiate(withOwner: nil, options: nil).first as? Self else {
            fatalError("Could not load view from nib file.")
        }
        return view
    }
}
