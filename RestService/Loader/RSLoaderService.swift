//
//  RSLoaderService.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import UIKit

public class RSLoaderService {
    
    public private(set) var loader : (UIView & RSLoaderViewProtocol)!
    public private(set) var isShown: Bool = false
    
    public static let shared: RSLoaderService = {
        let instance = RSLoaderService()
        instance.setup(loader: DefaultLoaderView.instantiateFromXib())
        return instance
    }()

    public func setup(loader view: (UIView & RSLoaderViewProtocol)) {
        DispatchQueue.main.async {
            self.loader = view
            self.loader.configure()
        }
    }
    
    public func show() {
        DispatchQueue.main.async {
            if !self.isShown {
                self.isShown = true
                self.loader.showView()
            }
        }
    }

    public func hide() {
        DispatchQueue.main.async {
            if self.isShown {
                self.isShown = false
                self.loader.hideView()
            }
        }
    }
}
