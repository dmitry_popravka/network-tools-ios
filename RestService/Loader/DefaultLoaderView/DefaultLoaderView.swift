//
//  DefaultLoaderView.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import UIKit

class DefaultLoaderView: UIView, RSLoaderViewProtocol {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    // MARK: -
    // MARK: configure
    func configure() {
        setup()
        addedConstraints()
        toBack()
    }
    
    /// Configure view and start animation
    func setup() {
        
        alpha = 0.0
        contentView.layer.cornerRadius = 10.0
        contentView.clipsToBounds = true

        if let windowView = UIApplication.shared.keyWindow {
            windowView.addSubview(self)
        }
    }
    
    /// Added constraints for view
    private func addedConstraints() {
        
        if let superview = self.superview {
            let sizeFrame: CGFloat = 120.0
            let width = UIScreen.main.bounds.width / 2.0 - sizeFrame / 2.0
            let height = UIScreen.main.bounds.height / 2.0 - sizeFrame / 2.0
            
            let centerXConstraint = NSLayoutConstraint(item: self,
                                                       attribute: .centerX,
                                                       relatedBy: .equal,
                                                       toItem: superview,
                                                       attribute: .centerX,
                                                       multiplier: 1,
                                                       constant: 0.0)
            
            let centerYConstraint = NSLayoutConstraint(item: self,
                                                       attribute: .centerY,
                                                       relatedBy: .equal,
                                                       toItem: superview,
                                                       attribute: .centerY,
                                                       multiplier: 1,
                                                       constant: 0.0)
            
            let widthConstraint = NSLayoutConstraint(item: self,
                                                     attribute: .width,
                                                     relatedBy: .equal,
                                                     toItem: nil,
                                                     attribute: .notAnAttribute,
                                                     multiplier: 1,
                                                     constant: width)
            
            let heightXConstraint = NSLayoutConstraint(item: self,
                                                       attribute: .height,
                                                       relatedBy: .equal,
                                                       toItem: nil,
                                                       attribute: .notAnAttribute,
                                                       multiplier: 1,
                                                       constant: height)
            
            translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([centerXConstraint, centerYConstraint, widthConstraint, heightXConstraint])
            
        }
        layoutIfNeeded()
        superview?.layoutIfNeeded()
    }
    
    // MARK: -
    // MARK: show
    func showView() {
        
        toFront()
        indicatorView.startAnimating()
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions(), animations: { () -> Void in
            self.alpha = 1.0
        }, completion: nil)
    }
    
    func toFront() {
        superview?.bringSubviewToFront(self)
    }
    
    // MARK: -
    // MARK: hide
    func hideView() {
        
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.alpha = 0.0
        }, completion: { (Bool) -> Void in
            
            self.toBack()
            self.indicatorView.startAnimating()
            
        })
    }
    
    private func toBack() {
        superview?.sendSubviewToBack(self)
    }
}
