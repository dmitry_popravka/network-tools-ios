//
//  RSParse.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import Foundation
import MobileCoreServices

struct RSParse<S: Codable, E: Codable> {

        func parse (handler: (data: Data?, response: URLResponse?, err: Error?),
                    success:@escaping (RSResponse<S>)->(),
                    error: @escaping (RSResponse<E>)->(),
                    failure:@escaping (Error)->()) {
            ///
            let statusCode = (handler.response as! HTTPURLResponse).statusCode
            let headerFields = (handler.response as! HTTPURLResponse).allHeaderFields 
            
            /// failure
            guard let data = handler.data, handler.err == nil else {
                failure(handler.err!)
                return
            }
     
            /// model
            if let successModel = convert(data: data, toModel: S.self) {
                
                let successResponse = RSResponse(model: successModel,
                                                 statusCode: statusCode,
                                                 headerFields: headerFields)
                success(successResponse)
                return
            }
            
            /// error
            if let errorModel = convert(data: data, toModel: E.self) {
                
                let errorResponse = RSResponse(model: errorModel,
                                               statusCode: statusCode,
                                               headerFields: headerFields)
                error(errorResponse)
                return
            }
            
            failure(RSErrorType.convertJSON(handler.response?.url?.absoluteString ?? "nil"))

    }
    
    
    // MARK: -
    // MARK: convert
    
    func convert <Model: Codable> (data: Data, toModel type: Model.Type) -> Model? {
        
        switch type {
        case is Data.Type:
            return data as? Model
        case is String.Type:
            return String(data: data, encoding: .utf8) as? Model
//        case is Int.Type:
//            return String(data: data, encoding: .utf8) as? Model
//        case is Double.Type:
//            return String(data: data, encoding: .utf8) as? Model
//        case is URL.Type:
//            return String(data: data, encoding: .utf8) as? Model
//        case is Array.Type:
//            return String(data: data, encoding: .utf8) as? Model
//        case is Dictionary.Type:
//            return String(data: data, encoding: .utf8) as? Model
        default:
            let decoder = JSONDecoder()

            do {
                return try decoder.decode(type, from: data)
                
            } catch let error {
//                DecodingError
                debugPrint(#function, error)
                return nil
            }
        }
    }
    
        func convert <Model: Codable> (json: String, toModel type: Model.Type) -> Model? {

        guard let data = json.data(using: .utf8) else {
            debugPrint(#function)
            return nil
        }
        
        return convert(data: data, toModel: type)
    }
    
    // MARK: -
    // MARK: other
    
    /// mime type for path
    func mimeType(forPath url: URL) -> String {
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, url.pathExtension as NSString, nil)?.takeRetainedValue(),
            let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
            return mimetype as String
        }
        return "application/octet-stream"
    }
}
