//
//  RSNotification.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import Foundation

public struct RSNotification {
    
    public struct Name {
       public static var start = NSNotification.Name(rawValue:"NSNotificationKeyRestCallStartLoading")
       public static var finish = NSNotification.Name(rawValue:"NSNotificationKeycallRestFinishLoading")
    }
    
   public struct Key {
       public static var name = "name"
       public static var loader = "loader"
    }
    
}
