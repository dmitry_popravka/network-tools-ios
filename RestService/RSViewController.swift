//
//  NetworkViewController.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import UIKit

open class RSViewController: UIViewController {
    
    public fileprivate(set) var restService: RestService!
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        restService = RestService(controllerName: controllerName)
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        restService.tapIgnoringAppear()
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        restService.tapIgnoringDisappear()
    }
    
    public var controllerName: String {
        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!
    }
}
