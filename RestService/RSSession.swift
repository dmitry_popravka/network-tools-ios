//
//  RSSession.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import Foundation

public class RSSession {

    public static let shared: RSSession = {
        let instance = RSSession()
        return instance
    }()

    public var isDebug: Bool = true
    public var failureBlock: ((_ error: Error)->())?
    
    public let session: URLSession = {
        
        let configuration = URLSessionConfiguration.default
        
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 60
        let headers: [String: String] = ["Content-Type": "application/json"]
        configuration.httpAdditionalHeaders = headers
        
        let session = URLSession(configuration: configuration)
        
        return session
    }()
    
    public func cancelCall(byName name: String) {
        
        RSSession.shared.session.getAllTasks { tasks in
            
            for task in tasks {
                if task.taskDescription == name {
                    
                    if self.isDebug {
                        RSPrint().printCancelCall(whereName: name,
                                                    sessionTask: task)
                    }
                    
                    task.cancel()
                    
                }
            }
        }
    }
    
}
