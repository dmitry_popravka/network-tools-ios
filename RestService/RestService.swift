//
//  RestService.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import UIKit

public class RestService {
    
    public fileprivate(set) var controllerName: String
    public fileprivate(set) var tapIgnoring: (isIgnoring: Bool,
                                              views: Set<UIView>,
                                              count: (all: Int, loader: Int)) = (false, [], (0, 0))

    init(controllerName name: String) {
        controllerName = name
        setupStartLoadingNotification()
        setupFinishLoadingNotification()
    }

    //MARK: -
    //MARK: start loading notification
    
    fileprivate func setupStartLoadingNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(startLoading(notification:)),
                                               name:RSNotification.Name.start,
                                               object: nil)
    }
    
    @objc private func startLoading(notification: Notification) {
        
        if let userInfo = notification.userInfo,
            let name = userInfo[RSNotification.Key.name] as? String,
            let loader = userInfo[RSNotification.Key.loader] as? Bool {
            
            if controllerName == name {
                
                if loader {
                    tapIgnoring.count.loader += 1
                    RSLoaderService.shared.show()
                }
                
                tapIgnoring.count.all += 1
                self.tapIgnoring(isTapIgnoring: true)
            }
        }
    }
    
    //MARK: -
    //MARK: finish loading notification
    
    fileprivate func setupFinishLoadingNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(finishLoading(notification:)),
                                               name: RSNotification.Name.finish,
                                               object: nil)
    }
    
    @objc func finishLoading(notification: Notification) {
        
        if let userInfo = notification.userInfo,
            let name = userInfo[RSNotification.Key.name] as? String,
            let loader = userInfo[RSNotification.Key.loader] as? Bool {
            
            if controllerName == name {
                
                if loader {
                    tapIgnoring.count.loader -= 1
                    if tapIgnoring.count.loader < 0 {
                        tapIgnoring.count.loader = 0
                    }
                }
                
                if tapIgnoring.count.loader == 0 {
                    RSLoaderService.shared.hide()
                }
                
                tapIgnoring.count.all -= 1
                if tapIgnoring.count.all < 0 { tapIgnoring.count.all = 0 }
                if tapIgnoring.count.all == 0 { tapIgnoring(isTapIgnoring: false) }
                
            }
        }
    }
    
    //MARK: -
    //MARK: tap ignoring
    
    func tapIgnoringAppear() {
        if tapIgnoring.count.all > 0 {
            if tapIgnoring.count.loader > 0 {
                RSLoaderService.shared.show()
            }
            self.tapIgnoring(isTapIgnoring: true)
        } else {
            RSLoaderService.shared.hide()
            self.tapIgnoring(isTapIgnoring: false)
        }
    }
    
    func tapIgnoringDisappear() {
        if tapIgnoring.count.all > 0 {
            RSLoaderService.shared.hide()
        }
    }
    
    public func tapIgnoring(add views: UIView?...) {
        for view in views {
            if let view = view {
                tapIgnoring.views.insert(view)
            }
        }
    }
    
    public func tapIgnoring(remove views: UIView?...) {
        for view in views {
            if let view = view {
                tapIgnoring.views.remove(view)
            }
        }
    }
    
    internal func tapIgnoring(isTapIgnoring status: Bool) {
        
        DispatchQueue.main.async {
            
            if self.tapIgnoring.isIgnoring == status { return }
            self.tapIgnoring.isIgnoring = status
            
            for view in self.tapIgnoring.views {
                view.isUserInteractionEnabled = !status
            }
        }
    }
    
    func tapIgnoring(cancelRequestName name: String) {
        RSSession.shared.cancelCall(byName: controllerName)
        tapIgnoring.count = (all: 0, loader: 0)
        tapIgnoring(isTapIgnoring: false)
        RSLoaderService.shared.hide()
    }
    
}
