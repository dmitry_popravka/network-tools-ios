//
//  RSPaginationProtocol.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import Foundation

public protocol RSPaginationProtocol {
    var totalPage: Int {get set}
    var currentPage: Int {get set}
    var isLoading: Bool {get set}
}

extension RSPaginationProtocol {
    
    mutating func merge(with model: RSPaginationProtocol?) {
        if let model = model {
            self.totalPage = model.totalPage
            self.currentPage = model.currentPage
        }
    }
    
    func canLoadNextPage() -> Bool {
        
        if totalPage <= currentPage {
            return false
        }
        
        return true
    }
}
