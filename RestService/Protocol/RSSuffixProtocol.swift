//
//  RSSuffixProtocol.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import Foundation

public protocol RSSuffixProtocol {
    
    associatedtype SuffixType : RawRepresentable where SuffixType.RawValue == String

    var urlLink: String {get}
    var versionUrlLink: String {get}
    
}

extension RSSuffixProtocol {
    
    public func baseUrl() -> String {
        let url = urlLink + versionUrlLink
        return url
    }
    
    public func path(suffix: SuffixType, params: String? = nil) -> String {
        
        var path = baseUrl() + suffix.rawValue
        if let params = params { path += params }

        return path.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    }
    
}
