//
//  RSCalls.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import UIKit

open class RSCalls<S: Codable, E: Codable> {
    
    func call(path: String,
              method: RSMethod,
              name: String,
              params: [String: Any]?,
              headers: [String: String]?,
              success:@escaping (RSResponse<S>)->(),
              error: @escaping (RSResponse<E>)->(),
              failure:@escaping (Error)->()){
        
        let url = URL(string: path)!
        var request = URLRequest(url: url)
        
        request.httpMethod = method.rawValue
        
        if let headers = headers {
            headers.forEach { (key, value) in
                request.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        if let params = params {
            let body = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            request.httpBody = body
        }
        
        let task = RSSession.shared.session.dataTask(with: request) { data, response, err in
            
            if RSSession.shared.isDebug {
                RSPrint().print(request: request,
                                params: params,
                                handler: (data, response, err))
            }
            
            RSParse<S, E>().parse(handler: (data, response, err),
                                  success: { result in
                                    success(result)
            },
                                  error: { errorModel in
                                    error(errorModel)
            },
                                  failure: { error in
                                    failure(error)
            })
        }
        task.taskDescription = name
        task.resume()
    }
    
    // MARK: -
    // MARK: media
    
    func callVideo(path: String,
                   method: RSMethod,
                   name: String,
                   params: [String: Any]?,
                   headers: [String:String]?,
                   videos: [String:URL],
                   success:@escaping (RSResponse<S>)->(),
                   error: @escaping (RSResponse<E>)->(),
                   failure:@escaping (Error)->()){
        
        //        RestCalls.sessionManager.upload(multipartFormData: { (multipartFormData) in
        //            for (key, value) in videos {
        //                /// video
        //                if let data = try? Data(contentsOf: value) {
        //                    multipartFormData.append(data, withName: key, fileName: "video.mov", mimeType: self.mimeType(forPath: value))
        //                }
        //                /// params
        //                if let params = params {
        //                    for (key, value) in params {
        //                        multipartFormData.append((value.data(using: .utf8))!, withName: key)
        //                    }
        //                }
        //            }
        //        }, to: path, method: RestMethod.toAlamofile(method), headers: headers, encodingCompletion: { (result) in
        //
        //            switch result {
        //            case .success(let upload, _, _):
        //                let request = upload.responseObject(queue: nil, keyPath: nil, mapToObject: nil, context: nil, completionHandler: { (response: DataResponse<Model>) in
        //                    // print
        //                    self.printResponse(response: response, params: params)
        //                    // parsing
        //                    self.parseResponse(response: response, success: { (result) in
        //                        success(result)
        //                    }, successError: { errorModel in
        //                        error(errorModel)
        //                    }, failure: { error in
        //                        failure(error)
        //                    })
        //                })
        //
        //                request.task?.taskDescription = name
        //
        //            case .failure(let encodingError):
        //                failure(encodingError)
        //            }
        //        })
    }
    
        func callImage(path: String,
                       method: RSMethod,
                       name: String,
                       params: [String: Any]?,
                       headers: [String:String]?,
                       images: [String:UIImage],
                       success:@escaping (RSResponse<S>)->(),
                       error: @escaping (RSResponse<E>)->(),
                       failure:@escaping (Error)->()){
    
    //        RestCalls.sessionManager.upload(multipartFormData: { (multipartFormData) in
    //
    //            /// params
    //            if let params = params {
    //                for (key, value) in params {
    //                    multipartFormData.append((value.data(using: .utf8))!, withName: key)
    //                }
    //            }
    //            /// images
    //            for (key, value) in images {
    //                let imageData = value.jpegData(compressionQuality: 0.7)
    //                multipartFormData.append(imageData!, withName: key, fileName: "picture.jpg", mimeType: "image/jpeg")
    //            }
    //
    //        }, to: path, method: RestMethod.toAlamofile(method), headers: headers, encodingCompletion: { (result) in
    //
    //            switch result {
    //            case .success(let upload, _, _):
    //
    //                let request = upload.responseObject(queue: nil, keyPath: nil, mapToObject: nil, context: nil, completionHandler: { (response: DataResponse<Model>) in
    //                    // print
    //                    self.printResponse(response: response, params: params)
    //                    // parsing
    //                    self.parseResponse(response: response, success: { (result) in
    //                        success(result)
    //                    }, successError: { errorModel in
    //                        error(errorModel)
    //                    }, failure: { error in
    //                        failure(error)
    //                    })
    //                })
    //
    //                request.task?.taskDescription = name
    //
    //            case .failure(let encodingError):
    //                failure(encodingError)
    //            }
    //        })
        }
    
    // MARK: -
    // MARK: download
    
    //    func downloadFile(path: String,
    //                      filename: String,
    //                      method: RestMethod,
    //                      name: String,
    //                      params: [String:Any]?,
    //                      headers: [String:String]?,
    //                      complate: @escaping (_ response: Any) -> ()) {
    
    //        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
    //            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    //            let fileURL = documentsURL.appendingPathComponent(filename)
    //
    //            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    //        }
    //
    //
    //        let request = RestCalls.sessionManager.download(path,
    //                                                        method: RestMethod.toAlamofile(method),
    //                                                        parameters: params,
    //                                                        headers: headers,
    //                                                        to: destination).response { response in
    //                                                            // print
    //                                                            self.printResponse(response: response, params: params)
    //                                                            // parsing
    //                                                            complate(response)
    //        }
    //
    //        request.task?.taskDescription = name
    //    }
    
}



