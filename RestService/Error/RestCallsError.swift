//
//  RestCallsError.swift
//  NetworkTools
//
//  Created by Dmitry on 2/14/19.
//  Copyright © 2019 mac-15. All rights reserved.
//

import Foundation

enum RestCallsError: Error {
    
    case convertJSON(String)
    case request(String)
    case whileMapping(String)
    case toJSONString
    
    var message: String {
        switch self {
        case .convertJSON(let value):
            return "No convert item to json: \n->>> \(value)"
        case .request(let value):
            return "Bad url: \n->>> \(value)"
        case .whileMapping(let value):
            return "Got an error while mapping: \n\(value)"
        case .toJSONString:
            return "Error json parsing"
        }
    }
    

}
