//
//  RSResponse.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import Foundation

public struct RSResponse<Model: Codable> {
    public var model: Model
    public var statusCode: Int
    public var headerFields: [AnyHashable : Any]
}
