//
//  RSOperation.swift
//  RestService
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import Foundation

open class RSOperation: NSObject {

    func notificationBeforeCall(name: String, loader: Bool)  {
        
        NotificationCenter.default.post(name: RSNotification.Name.start,
                                        object: nil,
                                        userInfo: [RSNotification.Key.name: name,
                                                   RSNotification.Key.loader: loader])
        
    }
    
    func notificationAfterCall(name: String, loader: Bool = true) {
        
        NotificationCenter.default.post(name: RSNotification.Name.finish,
                                        object: nil,
                                        userInfo: [RSNotification.Key.name: name,
                                                   RSNotification.Key.loader: loader])
    }
    
    // MARK: -
    // MARK: Model
    
    public func call <S: Codable,E: RSErrorProtocol> (successModel: S.Type,
                                                      errorModel: E.Type,
                                                      path: String,
                                                      method: RSMethod,
                                                      name: String,
                                                      params: [String:Any]? = nil,
                                                      headers: [String: String]? = nil,
                                                      loader: Bool = true,
                                                      success: @escaping (RSResponse<S>)->(),
                                                      error: ((RSResponse<E>)->())? = nil,
                                                      failure: ((Error)->())? = nil) {
        
        let apiCalls = RSCalls<S, E>()
        
        notificationBeforeCall(name: name, loader: loader)
        
        apiCalls.call(path: path,
                      method: method,
                      name: name,
                      params: params,
                      headers: headers,
                      success: { (response) in
                        success(response)
                        self.notificationAfterCall(name: name, loader: loader)
        },
                      error: { (response) in
                        
                        if let error = error {
                            error(response)
                        } else {
                            RSError().parsing(error: response.model)
                        }
                        
                        self.notificationAfterCall(name: name)
        },
                      failure:{ (error) in
                        
                        if let failure = failure {
                            failure(error)
                        } else {
                            RSError().parsing(failure:error)
                        }
                        
                        self.notificationAfterCall(name: name)
        })
    }
    
}
