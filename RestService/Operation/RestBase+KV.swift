//
//  RestBase+KV.swift
//  NetworkTools
//
//  Created by Dmitry on 1/29/19.
//  Copyright © 2019 mac-15. All rights reserved.
//

import Foundation

//extension RestBase {
//    func callKV <Model> (model: Model.Type,
//                         path: String,
//                         method: RestMethod,
//                         encoding: ParameterEncoding = JSONEncoding.default,
//                         name: String,
//                         params: [String:Any]? = nil,
//                         headers: [String: String]? = nil,
//                         loader: Bool = true,
//                         success: @escaping (_ model: Model, _ pagination: BasePaginationModel?)->(),
//                         error: (([ErrorModel])->())? = nil,
//                         failure: ((Error)->())? = nil) where Model: Mappable, Model: BaseModelProtocol {
//        
//        setupBeforeCall(name: name, loader: loader)
//        
//        apiCalls.callKV(model: model,
//                        path: path,
//                        method: method,
//                        encoding: encoding,
//                        name: name,
//                        params: params,
//                        headers: headers,
//                        success: { (result, pagination) in
//                            success(result, pagination)
//                            self.setupAfterCall(name: name, loader: loader)
//                            
//        },
//                        error: { (model) in
//                            if let error = error {
//                                error(model)
//                            } else {
//                                RestDebugError.parsingKVSuccessError(model)
//                            }
//                            self.setupAfterCall(name: name)
//        },
//                        failure:{ (error) in
//                            if let failure = failure {
//                                failure(error)
//                            } else {
//                                RestDebugError.parsingFailure(error)
//                            }
//                            self.setupAfterCall(name: name)
//        })
//    }
//    
//    func callKV <Type> (type: Type.Type,
//                        path: String,
//                        method: RestMethod,
//                        encoding: ParameterEncoding = JSONEncoding.default,
//                        name: String,
//                        params: [String:Any]? = nil,
//                        headers: [String: String]? = nil,
//                        loader: Bool = true,
//                        success: @escaping (_ model: Type, _ pagination: BasePaginationModel?)->(),
//                        error: (([ErrorKVModel])->())? = nil,
//                        failure: ((Error)->())? = nil) {
//        
//        setupBeforeCall(name: name, loader: loader)
//        
//        apiCalls.callKV(type: type,
//                        path: path,
//                        method: method,
//                        encoding: encoding,
//                        name: name,
//                        params: params,
//                        headers: headers,
//                        success: { (result, pagination) in
//                            success(result, pagination)
//                            self.setupAfterCall(name: name, loader: loader)
//        },
//                        error: { (model) in
//                            if let error = error {
//                                error(model)
//                            } else {
//                                RestDebugError.parsingKVSuccessError(model)
//                            }
//                            self.setupAfterCall(name: name)
//        },
//                        failure: { (error) in
//                            if let failure = failure {
//                                failure(error)
//                            } else {
//                                RestDebugError.parsingFailure(error)
//                            }
//                            self.setupAfterCall(name: name)
//        })
//    }
//}
