//
//  RestBase+Model.swift
//  NetworkTools
//
//  Created by Dmitry on 1/29/19.
//  Copyright © 2019 mac-15. All rights reserved.
//

import Foundation

//public extension RestBase {
//    func call <Model> (model type: Model.Type,
//                       path: String,
//                       method: RestMethod,
////                       encoding: ParameterEncoding = JSONEncoding.default,
//                       name: String,
//                       data: Data,
//                       headers: [String: String]? = nil,
//                       loader: Bool = true,
//                       success: @escaping (Model)->(),
//                       error: ((ErrorModel)->())? = nil,
//                       failure: ((Error)->())? = nil) where Model: BaseModelProtocol {
//        
//        setupBeforeCall(name: name, loader: loader)
//        
//        apiCalls.call(model: type,
//                      path: path,
//                      method: method,
////                      encoding: encoding,
//                      name: name,
//                      data: data,
//                      headers: headers,
//                      success: { (result) in
//                        success(result)
//                        self.setupAfterCall(name: name, loader: loader)
//        },
//                      error: { (model) in
//                        if let error = error {
//                            error(model)
//                        } else {
//                            RestDebugError.parsingSuccessError(model)
//                        }
//                        self.setupAfterCall(name: name)
//        },
//                      failure:{ (error) in
//                        if let failure = failure {
//                            failure(error)
//                        } else {
//                            RestDebugError.parsingFailure(error)
//                        }
//                        self.setupAfterCall(name: name)
//        })
//    }
//}
