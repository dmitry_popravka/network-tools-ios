//
//  httpbinSuccessModel.swift
//  network-tools-ios
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import Foundation

struct httpbinSuccessModel: Codable {
    
    let args: httpbinSuccessModelArgs
    
    struct httpbinSuccessModelArgs: Codable {
        let data: String
        let size: String
        let style, name: String
        let hOffset, vOffset: String
        let alignment: String
    }
}

//MARK: test
extension httpbinSuccessModel {
    
    static func get() -> String {
        return "?data=ClickHere&size=36&style=bold&name=text1&hOffset=250&vOffset=100&alignment=center"
    }
    
    static func post() -> [String: Any] {
        
        return  [ "data": "Click Here",
                  "size": 36,
                  "style": "bold",
                  "name": "text1",
                  "hOffset": 250,
                  "vOffset": 100,
                  "alignment": "center"]
    }
    
}
