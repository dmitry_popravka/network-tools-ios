//
//  httpbinErrorModel.swift
//  network-tools-ios
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//
import Foundation
import RestService

public struct httpbinErrorModel: RSErrorProtocol {
    
    public var message: String
    public var status: String
    public var code: Int?

    public func show() {
        debugPrint(#function)
    }
}
