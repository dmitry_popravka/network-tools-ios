//
//  ErrorKVModel.swift
//  RestService
//
//  Created by mac-15 on 2/4/19.
//  Copyright © 2019 mac-15. All rights reserved.
//

import Foundation

public class ErrorKVModel: NSObject {
    
    public var code: Int?
    public var message: String
    public var field: String?
    
    init(code: Int?, message: String) {
        self.code = code
        self.message = message
        super.init()
    }
    
//    required public init?(map: Map) {
//        do {
//            code = try? map.value("Code")
//            message = try map.value("Message")
//            field = try? map.value("Field")
//        } catch let err {
//            debugPrint(err)
//            return nil
//        }
//    }
//    public func mapping(map: Map) {}
//
}

