//
//  httpbinSuffix.swift
//  network-tools-ios
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//
import UIKit
import RestService

struct httpbinSuffix: RSSuffixProtocol {
    
    let urlLink: String = "https://httpbin.org/"
    let versionUrlLink: String = ""

    enum SuffixType: String {
        case delete = "delete" /// DELETE The request's DELETE parameters.
        case get = "get" /// GET The request's query parameters.
        case patch = "patch" /// PATCH The request's PATCH parameters.
        case post = "post" /// POST The request's POST parameters.
        case put = "put" /// PUT The request's PUT parameters.
    }
}



