//
//  NetworkSuffix.swift
//  network-tools-ios
//
//  Created by Dmitry on 1/29/19.
//  Copyright © 2019 mac-15. All rights reserved.
//

import UIKit
import RestService

struct NetworkSuffix: RSSuffixProtocol {
    
    let urlLink: String = "http://httpbin.org/"

    enum SuffixType: String {
        case test = "agreement/12"
    }
}



