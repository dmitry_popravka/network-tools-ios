//
//  httpbinOperation.swift
//  network-tools-ios
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import UIKit
import RestService

class httpbinOperation: RSOperation {
    
    func get(withСallName name: String,
              success: @escaping (httpbinSuccessModel)->()) {
        
        let path = httpbinSuffix().path(suffix: .get, params: httpbinSuccessModel.get())

        self.call(successModel: httpbinSuccessModel.self,
                  errorModel: httpbinErrorModel.self,
                  path: path,
                  method: .get,
                  name: name,
                  loader: true,
                  success: { (response) in
                    success(response.model)
        })
        
    }    
    
}
