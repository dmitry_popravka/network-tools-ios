//
//  ViewController.swift
//  network-tools-ios
//
//  Created by Dmitry Popravka on 3/28/19.
//  Copyright © 2019 Dmitry Popravka. All rights reserved.
//

import UIKit
import RestService

class ViewController: RSViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        httpbinOperation().get(withСallName: controllerName) { (model) in
            self.show(message: "\(model)")
        }
        
    }
    
    private func show(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
